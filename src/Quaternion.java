import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double aa;
   private double bb;
   private double cc;
   private double dd;
   private static final double THRESHOLD = 0.000001;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      aa = a;
      bb = b;
      cc = c;
      dd = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return aa;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return bb;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return cc;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return dd;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String result = "";
      result += aa;
      if (bb < 0){
         result += bb + "i";
      } else {
         result += "+" + bb + "i";
      }
      if (cc < 0){
         result += cc + "j";
      } else {
         result += "+" + cc + "j";
      }
      if (dd < 0){
         result += dd + "k";
      } else {
         result += "+" + dd + "k";
      }
      return result;
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      if (s == null){
         throw new IllegalArgumentException("String can't be null");
      }
      if (s.trim().length() == 0){
         throw new IllegalArgumentException("String can't be empty");
      }
      if (!s.endsWith("k")){
         throw new IllegalArgumentException("Provided argument " + s + " is not a valid representation of the Quaternion");
      }
      if (!(s.indexOf("i") < s.indexOf("j") && s.indexOf("j") < s.indexOf("k"))){
         throw new IllegalArgumentException("argument " + s + " is not valid string representation of a quaternion.");
      }

      String[] parts = s.split("(?=\\+|\\-)");

      if (parts.length < 4 || parts.length > 8){
         throw new IllegalArgumentException("provided argument " + s + " is not a valid string representation of quaternion.");
      }

      String[] terms = new String[4];

      int k = 0;
      for (int i = 0; i < parts.length; i++) {
         if(parts[i].endsWith("e")){
            terms[k] = parts[i] + parts[i+1];
            i++;
         } else{
            terms[k]=parts[i];
         }
         k++;
      }

      for (int i = 0; i < terms.length; i++) {
         if (terms[i].endsWith("i") || terms[i].endsWith("j") || terms[i].endsWith("k")){
            terms[i] = terms[i].substring(0, terms[i].length() - 1);
         }
      }

      double a, b, c, d;
      try{
         a = Double.parseDouble(terms[0]);
         b = Double.parseDouble(terms[1]);
         c = Double.parseDouble(terms[2]);
         d = Double.parseDouble(terms[3]);
      } catch (NumberFormatException e){
         throw new IllegalArgumentException("Provided argument " + s + " is not a valid representation of Quaternion");
      }
      return new Quaternion(a, b, c, d);
      
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Quaternion(this.aa, this.bb, this.cc, this.dd);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return (Math.abs(0 - this.aa) < THRESHOLD)
              && (Math.abs(0 -  this.bb) < THRESHOLD)
              && (Math.abs(0 - this.cc) < THRESHOLD)
              && (Math.abs(0 - this.dd) < THRESHOLD);
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(this.aa, -1 * this.bb, -1 * this.cc,  -1 * this.dd);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-1 * this.aa, -1 * this.bb, -1 * this.cc,  -1 * this.dd);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.aa + q.aa, this.bb + q.bb, this.cc + q.cc, this.dd + q.dd);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double r = this.aa * q.aa
               - this.bb * q.bb
               - this.cc * q.cc
               - this.dd * q.dd;

      double i = this.aa * q.bb
               + this.bb * q.aa
               + this.cc * q.dd
               - this.dd * q.cc;

      double j = this.aa * q.cc
               - this.bb * q.dd
               + this.cc * q.aa
               + this.dd * q.bb;

      double k = this.aa * q.dd
               + this.bb * q.cc
               - this.cc * q.bb
               + this.dd * q.aa;

      return new Quaternion(r, i, j, k);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.aa * r, this.bb * r, this.cc * r, this.dd * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      double n = (this.aa * this.aa + this.bb * this.bb + this.cc * this.cc + this.dd * this.dd);

      if (this.isZero()){
         throw new RuntimeException("Can't divide by zero");
      }

      return new Quaternion(this.aa/n,
              - 1 * this.bb / n,
              - 1 * this.cc / n,
              - 1 * this.dd / n);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      Quaternion m;
      try{
         m = q.inverse();
      } catch (RuntimeException e){
         throw new RuntimeException("Can't divide by zero");
      }
      return times(m);
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      Quaternion n;
      try{
         n = q.inverse();
      } catch (RuntimeException e){
         throw new RuntimeException("Can't divide by zero");
      }

      return n.times(this);
   }


   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if(qo instanceof Quaternion) {
         return Math.abs(this.aa - ((Quaternion) qo).getRpart()) < THRESHOLD &&
                 Math.abs(this.bb - ((Quaternion) qo).getIpart()) < THRESHOLD &&
                 Math.abs(this.cc - ((Quaternion) qo).getJpart()) < THRESHOLD &&
                 Math.abs(this.dd - ((Quaternion) qo).getKpart()) < THRESHOLD;
      } else {
         throw new ClassCastException("Not Quaternion " + qo);
      }
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return (this.times(q.conjugate())).plus(q.times(this.conjugate())).times(0.5);

   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {

      return Objects.hash(aa, bb, cc, dd);
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.aa * this.aa
              + this.bb * this.bb
              + this.cc * this.cc
              + this.dd * this.dd);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
//      System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2k"));
//      should evaluete to "-0.021-0.32i-430.0j-540.0k".
//      System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2kk"));
//      should cause exception.
      System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2k+"));
//      should cause exception.



//      System.out.println (valueOf("2+3i+4j+5k"));
//      System.out.println (valueOf("-2-3i-4j-5k"));
//      System.out.println (valueOf("-2.0e-3-4.0e-1i-5.0e-2j-6.0e-3k"));
//      System.out.println (valueOf("     "));
//      System.out.println (valueOf("2+3i+4j+5kk"));
//      System.out.println (valueOf("2+3i+4j+5k+"));
//      System.out.println (valueOf("2+3j+4i+5k"));
//      System.out.println(valueOf("-2.0e-3-4.0e-1i-5.0e-2j-6.0e-3k"));
//      System.out.println(valueOf("2.0e+2+3i+4j+5k"));
//      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
//      if (arg.length > 0)
//         arv1 = valueOf (arg[0]);
//      System.out.println ("first: " + arv1.toString());
//      System.out.println ("real: " + arv1.getRpart());
//      System.out.println ("imagi: " + arv1.getIpart());
//      System.out.println ("imagj: " + arv1.getJpart());
//      System.out.println ("imagk: " + arv1.getKpart());
//      System.out.println ("isZero: " + arv1.isZero());
//      System.out.println ("conjugate: " + arv1.conjugate());
//      System.out.println ("opposite: " + arv1.opposite());
//      System.out.println ("hashCode: " + arv1.hashCode());
//      Quaternion res = null;
//      try {
//         res = (Quaternion)arv1.clone();
//      } catch (CloneNotSupportedException e) {};
//      System.out.println ("clone equals to original: " + res.equals (arv1));
//      System.out.println ("clone is not the same object: " + (res!=arv1));
//      System.out.println ("hashCode: " + res.hashCode());
//      res = valueOf (arv1.toString());
//      System.out.println ("string conversion equals to original: "
//         + res.equals (arv1));
//      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
//      if (arg.length > 1)
//         arv2 = valueOf (arg[1]);
//      System.out.println ("second: " + arv2.toString());
//      System.out.println ("hashCode: " + arv2.hashCode());
//      System.out.println ("equals: " + arv1.equals (arv2));
//      res = arv1.plus (arv2);
//      System.out.println ("plus: " + res);
//      System.out.println ("times: " + arv1.times (arv2));
//      System.out.println ("minus: " + arv1.minus (arv2));
//      double mm = arv1.norm();
//      System.out.println ("norm: " + mm);
//      System.out.println ("inverse: " + arv1.inverse());
//      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
//      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
//      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
